from django.urls import path
from receipts.views import receipt_list, createreceipt, category_list, account_list, createcategory, createaccount

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", createreceipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", createaccount, name="create_account"),
    path("categories/create/", createcategory, name="create_category"),
]
