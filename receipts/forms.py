from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class create_receipt(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class create_category(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
         "name",
        ]


class create_account(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
