from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from receipts.models import ExpenseCategory, Account, Receipt
from accounts.forms import login_form
from receipts.forms import create_receipt, create_category, create_account
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/home.html", context)


@login_required
def createreceipt(request):
    if request.method == "POST":
        form = create_receipt(request.POST)
        if form.is_valid():
            Receipt = form.save(False)
            Receipt.purchaser = request.user
            Receipt.save()
            return redirect("home")
    else:
        form = create_receipt()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def createcategory(request):
    if request.method == "POST":
        form = create_category(request.POST)
        if form.is_valid():
            Category = form.save(False)
            Category.owner = request.user
            Category.save()
            return redirect("category_list")
    else:
        form = create_category()

    context = {
        "form": form,
    }
    return render(request, "receipts/createcategory.html", context)


@login_required
def createaccount(request):
    if request.method == "POST":
        form = create_account(request.POST)
        if form.is_valid():
            Account = form.save(False)
            Account.owner = request.user
            Account.save()
            return redirect("account_list")
    else:
        form = create_account()

    context = {
        "form": form,
    }
    return render(request, "receipts/createaccount.html", context)

#final submission
